const app = require('./app.js')
const package_json = require('./package.json')

const PORT = 3000
const SERVICE_NAME = package_json.name

app.listen(PORT, () => {
  console.log(`${SERVICE_NAME} is listening on port ${PORT}`)
})
