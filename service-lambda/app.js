const Express = require('express')
const axios = require('axios')

const app = Express()

app.get('/', (req, res) => {
  retry({
    method: 'get',
    url: 'http://localhost:3001/ping'
  })
    .then(response => res.sendStatus(response.status))
    .catch(err => {
      console.error(err)
      return res.sendStatus(503)
    })
})

const retry = async (req_params) => {
  const RETRIABLE_STATUSES = [503]
  const MAX_RETRIES = 3

  let wait_ms = 1000

  for (let i = 0; i < MAX_RETRIES; i++) {
    try {
      return await axios(req_params)
    } catch (err) {
      if (err.response) {
        const is_retriable = RETRIABLE_STATUSES.includes(err.response.status)

        if (is_retriable) {
          await delay(wait_ms)
          wait_ms *= 2

          continue
        }
      }

      throw err
    }
  }
}

const delay = ms =>
  new Promise(resolve => setTimeout(resolve, ms))

module.exports = app
