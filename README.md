## Retry example

### Description

An example of a retry pattern (with the exponential back-off strategy).

### Running the example

- Open a terminal and go to `service-lambda` folder.
- Run `$ npm start`, - the λ service will run on `localhost:3000`

- Open a terminal and go to `service-tau` folder.
- Run `$ npm start`, - the τ service will run on `localhost:3001`

- Send a request: `GET localhost:3000/`,
  e.g. `$ curl "${LOCALHOST}:3000/"`

- The λ service will make 3 calls to the τ service that will only succeed
on the third attempt.

