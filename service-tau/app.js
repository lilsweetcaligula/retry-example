const Express = require('express')

const app = Express()

const makePingHandler = (() => {
  const SUCCEED_EACH_NTH = 3

  const withCallId = (res, { call_id }) =>
    res.set('x-call-id', call_id)

  let num_calls = 0

  return (req, res) => {
    num_calls++

    if (num_calls > 0 && num_calls % SUCCEED_EACH_NTH === 0) {
      return withCallId(res, { call_id: num_calls })
        .sendStatus(204)
    }

    return withCallId(res, { call_id: num_calls })
      .sendStatus(503)
  }
})

app.get('/ping', makePingHandler())

module.exports = app
